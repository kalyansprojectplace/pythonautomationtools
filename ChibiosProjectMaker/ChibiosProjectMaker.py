#! /usr/bin/python3
import os
import shutil
from QtCreatorUserFileParser import QtCreatorUserFileParser
import subprocess

class ChibiosProjectMaker(object):
    def __init__(self,
                 projectName:str,
                 projectFolder:str,
                 projectSettings:dict):
        """
        Creates a folder and move some files that are required for compiling and opening the project in QtCreator.
        Parameters
        ----------
        projectName : str
            Name of the project
        projectFolder : str
            Folder that the project needs to be created in
            Note that the folder's name will be same as the project name
        projectSettings : dict
            Contains project settings such as ChibiOS path for editing makefiles.

        """
        # Store variables
        self.__projectName = projectName
        self.__projectFolder = projectFolder
        self.__projectSettings = projectSettings
        # The file path of this file
        self.__thisFilePath = os.path.dirname(__file__)
        # Perform initial setup
        returnValue = self.__initialSetup()
        # Only perform next steps if we have been successful so far
        if(returnValue):
            # Move the files required for a small project
            self.__moveTemplateFiles()
            # We need to edit Makefile to write DEPDIR string for creating dep files
            self.__editAndWriteMakeFile()
            # Next modify and write the qtcreator.user file required to set the build configurations correctly.
            QtCreatorUserFileParser(projectName=projectName,
                                    projectPath=self.__projectPath,
                                    openOCDPath=self.__projectSettings['openocd'])
            # Finally run the python file.
            subprocess.Popen(["python", "DepFolderParser.py"],
                             cwd=self.__projectPath)


    def __initialSetup(self):
        """
        Creates a folder in the given project folder with the name of the project.
        Returns
        -------
        True if successful
        """
        # Create a folder with the same name as the projectName
        self.__projectPath = os.path.join(self.__projectFolder, self.__projectName)
        # Create a folder making sure that it already exists
        try:
            os.mkdir(self.__projectPath)
            return True
        except FileExistsError:
            print("The required folder already exists. Please choose another folder.")
            return False

    def __moveTemplateFiles(self):
        """
        Moves files
        1) chconf.h
        2) halconf.h
        3) main.c
        4) usbcfg.c
        5) usbcfg.h
        6) mcuconf.h
        into the folder destination folder
        Returns
        -------

        """
        srcChconf = os.path.join(self.__thisFilePath, "TemplateFiles",  "chconf.h")
        srcHalconf = os.path.join(self.__thisFilePath, "TemplateFiles",  "halconf.h")
        srcMainC = os.path.join(self.__thisFilePath, "TemplateFiles",  "main.c")
        srcMcuConf = os.path.join(self.__thisFilePath, "TemplateFiles",  "mcuconf.h")
        srcUsbCfg = os.path.join(self.__thisFilePath, "TemplateFiles",  "usbcfg.h")
        srcUsbCfgSrc = os.path.join(self.__thisFilePath, "TemplateFiles",  "usbcfg.c")
        srcReadMe = os.path.join(self.__thisFilePath, "TemplateFiles",  "Readme.md")
        srcPythonFile = os.path.join(self.__thisFilePath, "DepFolderParser.py")
        # Destination path
        dstChconf = os.path.join(self.__projectPath, "chconf.h")
        dstHalconf = os.path.join(self.__projectPath, "halconf.h")
        dstMainC = os.path.join(self.__projectPath, "main.c")
        dstMcuConf = os.path.join(self.__projectPath, "mcuconf.h")
        dstUsbCfg = os.path.join(self.__projectPath, "usbcfg.h")
        dstUsbCfgSrc = os.path.join(self.__projectPath, "usbcfg.c")
        dstReadMe = os.path.join(self.__projectPath, "Readme.md")
        dstPythonFile = os.path.join(self.__projectPath, "DepFolderParser.py")
        # List of src paths
        srcPathList = [srcChconf, srcHalconf, srcMainC, srcMcuConf, srcUsbCfg,
                       srcUsbCfgSrc, srcReadMe, srcPythonFile]
        dstPathList = [dstChconf, dstHalconf, dstMainC, dstMcuConf, dstUsbCfg,
                       dstUsbCfgSrc, dstReadMe, dstPythonFile]
        for i in range(len(srcPathList)):
            shutil.copyfile(srcPathList[i], dstPathList[i])

    def __editAndWriteMakeFile(self):
        """
        Edits and writes the make file to the project folder.
        """
        # Open the file in TemplateFiles folder
        srcMakeFilePath = os.path.join(self.__thisFilePath, "TemplateFiles", "Makefile")
        finalMakeFilesList = []
        lineToAppend = r"DEPDIR = dep"
        with open(srcMakeFilePath, 'r') as fileObj:
            linesList = fileObj.readlines()
            # Insert a line after we found `PROJECT = `
            for line in linesList:
                if("PROJECT = " in line):
                    finalMakeFilesList.append(lineToAppend)
                    finalMakeFilesList.append("\n")
                elif("CHIBIOS = " in line):
                    line = "CHIBIOS = " + self.__projectSettings['chibiosfolder']
                finalMakeFilesList.append(line)
        fileObj.close()
        # Write the finalMakeFilelist into Makefile
        dstMakeFilePath = os.path.join(self.__projectPath, "Makefile")
        with open(dstMakeFilePath, 'w') as wFileObj:
            for line in finalMakeFilesList:
                wFileObj.write(line)
        wFileObj.close()

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("projectname", help="The name of the project.")
    parser.add_argument("projectfolder", help="Folder where the project should be created.")
    thisFilePath = os.path.dirname(__file__)
    # Check if the settings file is present in the folder
    settingsFilePath = os.path.join(thisFilePath,
                                    "ChibiOSProjectMakerSettings.txt")
    with open(settingsFilePath, 'r') as settingsReadFObj:
        linesList = settingsReadFObj.readlines()
    settingsReadFObj.close()
    chibiosDir = linesList[0]
    args = parser.parse_args()
    chibosProjMaker = ChibiosProjectMaker(
            projectName=args.projectname,
            projectFolder=args.projectfolder,
            projectSettings=
                {'chibiosfolder' : chibiosDir,
                 'openocd':linesList[1]})


## ChibiOS project maker
A Small automation tool that will create a folder and move necessary files to start working with QtCreator for Chibios based projects. 

### Pre-requisites
Before using this tool, there are some things necessary to be set up in QtCreator. 

#### Switch off ClangCodeModel
The ClangCodeModel will not work for our current configuration and will notify you with hundreds of faults if you enable it.
You can disable it by going to  
`Help -> About Plugins -> ClangCodeModel`
You may need to restart the IDE for changes to take effect.
#### Enable BareMetal support
The BareMetal support can be enabled in the same place as the ClangCodeModel has been disabled in 
`Help -> About Plugins -> Device Support`
#### Set up Kit
To compile and debug Chibios projects, you have to let the IDE know which compiler and debugger to use to compile and debug the code.
You can easily add a custom kit in QtCreator. 
##### Adding debugger
`Tools -> Options -> Kits -> Debuggers`
select Add and choose a name for your debugger and the path to the 
`arm-none-eabi-gdb-py` in ARM toolchain you downloaded or the one that came along with Chibios.
##### Adding Compiler
`Tools -> Options -> Kits -> Compilers`

select `Add -> GCC -> C++` and choose a name for your compiler and the path to the 
`arm-none-eabi-gdb-c++` in ARM toolchain you downloaded or the one that came along with Chibios.
The same can be done for 
`Add -> GCC -> C` and choose `arm-none-eabi-gdb-gcc`.
##### Adding Kit
Now go to `Tools -> Options -> Kits -> Kits` and select Add. Make sure that you select the Compilers that you have created in the previous steps. It should look like this:

![qtkit](QtCreator_Kit_Final.png "BareMetal kit final configuration")

#### Running the script
Now you can run the script. If you don't want every time to go to the folder where you saved the script to run it, you can easily add the script folder to path.
`gedit ~/.bashrc`

add the below line to the end of script

`export PATH=/path/to/script/folder:$PATH`

and run

`source ~/.bashrc` in terminal to source your changes for the current terminal.

Then you can easily run the script from every terminal.

Edit the `ChibiOSProjectMakerSettings.txt` with Chibios folder and OpenOCD folder. These settings will be used to write the QtCreator settings file, so make sure they are correct.
After making sure that the path to Chibios and OpenOCD is correct, run the script using 

`ChibiosProjectMaker "ProjectName" "Folder/to/create/the/project/in"`

Open the folder in QtCreator using import existing project
option. 



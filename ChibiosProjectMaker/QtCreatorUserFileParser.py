from xml.etree import ElementTree
import os

class QtCreatorUserFileParser(object):
    def __init__(self,
                 projectPath:str,
                 projectName:str,
                 openOCDPath:str):
        """
        Parse and modified the QtCreator user file
        Parameters
        ----------
        projectPath : Path to the created project
        projectName : Name of the project
        openOCDPath : Path to openocd folder.
        """
        # This file Dir
        self.__thisFileDir = os.path.dirname(__file__)
        self.__projectPath = projectPath
        self.__projectName = projectName
        self.__openOCDFolder = openOCDPath
        self.parseQtCreatorFile()

    def parseQtCreatorFile(self):
        buildDirKey = "ProjectExplorer.BuildConfiguration.BuildDirectory"
        bareMetalExec = "BareMetal.CustomRunConfig.Executable"
        # OpenOCD
        openocdExecKey = "ProjectExplorer.CustomExecutableRunConfiguration.Executable"
        openocdRunConfig = "RunConfiguration.Arguments"
        creatorTemplateFile = os.path.join(self.__thisFileDir, "TemplateFiles", "Template.creator.user")
        document = ElementTree.parse(creatorTemplateFile)
        root = document.getroot()
        for key in root.iter('value'):
            keyAttrib = key.attrib
            try:
                if(keyAttrib['key'] == buildDirKey):
                     key.text = self.__projectPath
                elif(keyAttrib['key'] == bareMetalExec):
                    key.text = self.__projectPath + '/build/' + self.__projectName + '.elf'
                elif(keyAttrib['key'] == openocdExecKey):
                    key.text = self.__openOCDFolder + "/bin/openocd"
                elif(keyAttrib['key'] == openocdRunConfig):
                    key.text = self.__openOCDFolder + '/scripts/board/stm32f4discovery.cfg'
            except KeyError:
                pass
        # Write to a file
        destFilePath = os.path.join(self.__projectPath, self.__projectName+".creator.user")
        document.write(destFilePath, encoding='utf-8', xml_declaration=True)

if __name__ == '__main__':
    import shutil
    folderToWalk = r"/home/kalyansreenivas/Projects/chibios_practice_programs"
    thisFilePath = os.path.dirname(__file__)
    print(thisFilePath)
    listDirs = os.listdir(folderToWalk)
    for dir in listDirs:
        if(dir.startswith('.')):
            pass
        else:
            dirPath = os.path.join(folderToWalk, dir)
            if(os.path.isdir(dirPath)):
                creator = QtCreatorUserFileParser(
                    projectName=dir,
                    projectPath=dirPath,
                    openOCDPath="/home/kalyansreenivas/Compilers/openocd"
                )
                srcPythonFile = os.path.join(thisFilePath, "DepFolderParser.py")
                dstPythonFile = os.path.join(dirPath, "DepFolderParser.py")
                shutil.copyfile(srcPythonFile, dstPythonFile)
    # creator = QtCreatorUserFileParser(
    #     projectPath=r"/home/kalyansreenivas/Downloads/Temp/Hello",
    #     projectName="Hello",
    #     openOCDPath="/home/kalyansreenivas/Compilers/openocd"
    # )
